# 
#  RailsHelper.rb
#  MSO Intern
#  
#  Created by Stefan Diesing on 2012-05-16.
#  Copyright 2012 Stefan Diesing. All rights reserved.
# 

# TODO
# request with a proxy

require 'net/http'
require 'net/https'

# keep this module as general es posible so it can be useful for other rails apps
module RailsHelper
  
  USERAGENT = "Mozilla/5.0 RailsAppHelper"
  
  # Get the token as well as the session cookie
  # optional cookie can be passed to stay in a session
  # returns [token, cookie]
  def get_token(url, cookie = "")
    response = https_get(url,cookie)
    response[1] =~ /\<input name\=\"authenticity_token\" type\=\"hidden\" value\=\"(.*)\" \/\>/
    response[0]['set-cookie'] = "" unless response[0]['set-cookie']
    return [$1, response[0]['set-cookie'].split(';')[0]]
  end

  # logs into an rails app
  # on sucsees returns the session_id cookie as string
  def login(url, username, password)
    token = get_token(url)
    response = https_post url, "authenticity_token=#{}&user[login]=#{username}&user[password]=#{password}", token[0], token[1]
    raise "No redirect occured: server did not accept the request." unless response[0]["status"] == "302"
  
    return response[0]['set-cookie'].split(';')[0]
  end
  
  # send an https get request
  # returns metainfo and the body as an array
  def https_get(url, cookie = "")
    meta, body = net_http(url, true).request_get url, 'User-Agent' => USERAGENT, 'Cookie' => cookie
    return [meta, body]
  end

  # send an https post request with data
  # returns metainfo and the body as an array
  def https_post(url, data, token = nil, cookie = "")
    token = get_token(url,cookie).first unless token
    resp, body = net_http(url, true).request_post url, data + "&authenticity_token=#{token}", 'User-Agent' => USERAGENT, 'Cookie' => cookie
    return [resp, body]
  end
  
  # as the server uses ssl and the ca verify fails
  private
  
  def net_http(url, ssl = false)
    uri = URI.parse url
    http = Net::HTTP.new(uri.host, uri.port)
    if ssl
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    
    return http
  end
  
end
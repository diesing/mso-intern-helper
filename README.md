MSO Intern Helper
=================

MSO Intern Helper kann sich anmelden, die Kurse und Vertretungsstunden auslesen.

Wie funktioniert das?
---------------------

Ganz simpel.

#### Anmelden
`mso = MSOIntern.new 'benutzername', 'passwort' `

#### Vertretungsstunden
`mso.coverlessons`

#### Kurse
`mso.courses`
# 
#  example.rb
#  MSOIntern
#  
#  Created by Stefan Diesing on 2012-05-16.
#  Copyright 2012 Stefan Diesing. All rights reserved.
# 

require_relative 'lib/MSOIntern'

USERNAME = ""
PASSWORD = ""

# login
mso = MSOIntern.new USERNAME, PASSWORD 

# get the coverlessons
puts mso.coverlessons.map{|e| e.join("\t")}

# get your courses
puts mso.courses.map{|e| e.join("\t")}